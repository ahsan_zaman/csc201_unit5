/**
 * 
 * @author Ahsan Zaman
 *
 * Algorithm:
 * 1. Declare JPanel name Panel1 and set layout to GridLayout with 2 rows and 2 columns
 * 2. Add 3 JButton objects to panel1
 * 3. Declare JPanel named panel2 and set layout to GridLayout with 2 rows and 2 columns
 * 4. Add 3 JButton objects to panel2
 * 5. Create JFrame object frame and set it to visible, size to (700, 700), layout to BorderLayout
 * 6. Add panel2 to the center of the frame
 * 7. Add panel1 to the south of the frame
 * END
 */

import javax.swing.*;

import java.awt.*;
public class GridLayoutDemo extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panel1;
	private JPanel panel2;
	private JFrame frame;
	public final int ROWS=2;
	public final int COLUMNS=2;
	public GridLayoutDemo(){
		// Creating panel1 and setting it to gridlayout with rows and columns as 2, 2
		panel1 = new JPanel( new GridLayout( ROWS, COLUMNS ) );
		panel1.add( new JButton( "Button 1" ) );
		panel1.add( new JButton( "Button 2" ) );
		panel1.add( new JButton( "Button 3" ) );
		
		// creating panel2 and setting to gridlayout with rows and columns as 2, 2
		panel2 = new JPanel( new GridLayout( ROWS, COLUMNS ) );
		panel2.add( new JButton( "Button 4" ) );
		panel2.add( new JButton( "Button 5" ) );
		panel2.add( new JButton( "Button 6" ) );

		frame = new JFrame( "GridLayout Demo" );
		frame.setVisible( true );
		frame.setSize( 700, 700 );
		frame.setLayout( new BorderLayout() );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		// Adjusting the panels so that panel2 is on top and panel1 is lower.
		frame.add( panel2, BorderLayout.CENTER );
		frame.add( panel1, BorderLayout.SOUTH );
		frame.pack();
	}
	
	public static void main( String[] args ){
		new GridLayoutDemo();
	}
}
