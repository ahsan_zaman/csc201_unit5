/**
 * 
 * @author Ahsan Zaman
 *
 * Algorithm:
 * 1. Create a JFrame object and title it "Parabola"\
 * 2. Set it visible, and its size to (300, 300).
 * 3. Add a GraphPanel object to the frame.
 * 4. Create a JPanel panel in GraphPanel.
 * 5. Override the paintComponent method with Graphics variable g set as a parameter
 * 6. Add points to the polygon p
 * 7. Draw Parabola using DrawPolyline method through Graphics variable g;
 * 8. Draw the String "Parabola Figure" using DrawString through g
 * END
 */

import javax.swing.*;
import java.awt.*;

public class Parabola extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public JFrame frame;
	public final String GRAPH_LABEL = "Parabola Figure";
	public class GraphPanel extends JPanel{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		public JPanel panel;
		public GraphPanel(){
			panel = new JPanel();
		}
		/**
		 * @param g - graphics variable for using the drawing methods in Graphics
		 * 
		 */
		protected void paintComponent( Graphics g ){
			/** scaleFactor - Smaller the scale factor the better the curve of the parabola*/
			double scaleFactor=0.1;
			Polygon p = new Polygon();
			for (int x=-100; x<=100; x++) {
				/** addPoint - Adding x, y points to Polygon p*/
				p.addPoint( x+200, 200- (int)(scaleFactor *x *x ) );
			}
			/** drawPolyline - Drawing the parabola using the points in Polygon p*/
			g.drawPolyline( p.xpoints, p.ypoints, p.npoints );
			/** drawString - Labeling the figure with a string*/
			g.drawString( GRAPH_LABEL , 30, 30 );
		}
	}
	
	public Parabola(){
		frame = new JFrame( "Parabola" );
		frame.setVisible( true );
		frame.setSize( 300, 300 );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.add( new GraphPanel() );
	}
	
	public static void main( String[] args ){
		new Parabola();
	}
}
