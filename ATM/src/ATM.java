/**
 * 
 * @author Ahsan Zaman
 *
 * Algorithm:
 * 1. Declare and Initialize an array of 10 JButton objects as keypad
 * 2. Declare and Initialize a JPanel panel for the keypad
 * 3. Declare and initialize a password display
 * 4. Declare and initialize a JPanel panel for password display
 * 5. Declare and initialize an enter and clear button for keypad
 * 6. Set up the user interface using these components in JFrame
 * 7. Set up an action listener for each of the buttons
 * END 
 */
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
public class ATM extends JFrame{
	/**
	 * serialVersionUID - To avoid warnings that would otherwise be generated.
	 */
	private static final long serialVersionUID = 1L;
	private final int KEYS=10;
	private final int ROWS=4;
	private final int COLUMNS=3;
	
	protected JPasswordField passwordDisplay;
	protected JButton jbtnKeypad[];
	protected JPanel jpnlKeypad;
	protected JPanel jpnlDisplay;
	protected JButton enterButton;
	protected JButton clearButton;
	protected int displayValue;
	protected int counter;
	private final int correctPIN=1234;
	
	public ATM(){
		counter=0;
		passwordDisplay = new JPasswordField(KEYS);
		jbtnKeypad = new JButton[KEYS];
		enterButton = new JButton( "ENTER" );
		clearButton = new JButton( "CLEAR" );
		/**
		 * jpnlKeypad - Gridlayout keypad with 4 rows and 3 columns
		 */
		jpnlKeypad = new JPanel( new GridLayout( ROWS, COLUMNS ) );
		jpnlDisplay = new JPanel();
		
		/**
		 * Creating listeners for each of the keys in the keypad
		 */
		ButtonAction keypadListener = new ButtonAction();
		ClearAction clearListener = new ClearAction();
		EnterAction enterListener = new EnterAction();
		
		jpnlDisplay.add( passwordDisplay );
		setLayout( new BorderLayout() );
		setVisible( true );
		setLocation( 400, 100 );
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		setTitle( "ATM PIN AUTHENTICATION " );
		setSize( 800, 600 );
		
		/**
		 * Establishing the keypad in the correct order.
		 * The standard phone keypad layout is used.
		 * The appropriate listener is also added to each button.
		 */
		for( int i=1; i<10 ;i++ ){
			jbtnKeypad[i] = new JButton( ""+i );
			jbtnKeypad[i].addActionListener( keypadListener );
			jpnlKeypad.add( jbtnKeypad[i] );
		}
		
		clearButton.addActionListener( clearListener );
		jpnlKeypad.add( clearButton );
		
		jbtnKeypad[0] = new JButton( ""+0 );
		jbtnKeypad[0].addActionListener( keypadListener );
		jpnlKeypad.add( jbtnKeypad[0] );
		
		enterButton.addActionListener( enterListener );
		jpnlKeypad.add( enterButton );
		
		add( jpnlDisplay, BorderLayout.NORTH );
		add( jpnlKeypad, BorderLayout.CENTER );
		
	}
	
	public static void main( String[] args ){
		new ATM();
	}
	
	public class ButtonAction implements ActionListener{
		/**
		 * actionPerformed - Overriding ActionListener method to use it
		 */
		public void actionPerformed( ActionEvent e ){
			if ( e.getActionCommand().equals("0") ) {
				passwordDisplay.setText( passwordDisplay.getText() + "0" );
			} else if (e.getActionCommand().equals("1")) {
				passwordDisplay.setText( passwordDisplay.getText() + "1" );
			} else if (e.getActionCommand().equals("2")) {
				passwordDisplay.setText( passwordDisplay.getText() + "2" );
			} else if (e.getActionCommand().equals("3")) {
				passwordDisplay.setText( passwordDisplay.getText() + "3" );
			} else if (e.getActionCommand().equals("4")) {
				passwordDisplay.setText( passwordDisplay.getText() + "4" );
			} else if (e.getActionCommand().equals("5")) {
				passwordDisplay.setText( passwordDisplay.getText() + "5" );
			} else if (e.getActionCommand().equals("6")) {
				passwordDisplay.setText( passwordDisplay.getText() + "6" );
			} else if (e.getActionCommand().equals("7")) {
				passwordDisplay.setText( passwordDisplay.getText() + "7" );
			} else if (e.getActionCommand().equals("8")) {
				passwordDisplay.setText( passwordDisplay.getText() + "8" );
			} else if (e.getActionCommand().equals("9")) {
				passwordDisplay.setText( passwordDisplay.getText() + "9");
			}
		}
	}
	
	public class ClearAction implements ActionListener{
		/**
		 * actionPerformed - Overriding actionlistener method to use it
		 */
		public void actionPerformed( ActionEvent e ){
			// When clear button is clicked
			if( e.getActionCommand().equals("CLEAR") ){
				passwordDisplay.setText( "" );
				displayValue=0;
			}
		}
	}
	
	public class EnterAction implements ActionListener{
		/**
		 * actionPerformed - Overriding actionlistener method to use it
		 */
		public void actionPerformed( ActionEvent e ){
			// When enter button is clicked
			if( e.getActionCommand().equals( "ENTER" ) ){
				// A total of four incorrect tries are given to the user
				if( counter<4 ){
					displayValue=Integer.parseInt( passwordDisplay.getText() );
					if( displayValue == correctPIN ){
						JOptionPane.showMessageDialog( null, "Welcome!" );
						passwordDisplay.setText( "" );
					}
					else {
						counter++;
						passwordDisplay.setText( "" );
						JOptionPane.showMessageDialog( null, "Tries left: "+(4-counter));
					}
				}
				else JOptionPane.showMessageDialog( null, "All out of tries. \nPlease contact your respective "
						+ "admin.");
				
			}
		}
	}
	
}
