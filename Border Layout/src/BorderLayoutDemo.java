/**
 * 
 * @author Ahsan Zaman
 * CSC 201 - Unit 5
 * BorderLayout Demo
 * 
 * Algorithm:
 * 1. Declare and initialize JFrame object
 * 2. Declare and initialize two JPanel objects
 * 3. Set the frame to be visible, the size, layout and default close operation.
 * 4. Add three JButton objects to panel1.
 * 5. Add three JButton objects to panel2.
 * 6. Add panel1, panel2 to the frame.
 * END
 */
import javax.swing.*;

import java.awt.*;
public class BorderLayoutDemo extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panel1;
	private JPanel panel2;
	private JFrame frame;
	
	public BorderLayoutDemo(){
		// Creating a JPanel panel and adding buttons
		panel1 = new JPanel( new FlowLayout() );
		panel1.add( new JButton( "Button 1" ) );
		panel1.add( new JButton( "Button 2" ) );
		panel1.add( new JButton( "Button 3" ) );
		
		/** tempPanel - Temporary panel that will contain both panel1 and panel2*/
		JPanel tempPanel = new JPanel( new BorderLayout() );
		// Creating second panel and adding buttons
		panel2 = new JPanel( new FlowLayout() );
		panel2.add( new JButton( "Button 4" ) );
		panel2.add( new JButton( "Button 5" ) );
		panel2.add( new JButton( "Button 6" ) );
		tempPanel.add( panel2, BorderLayout.CENTER );
		tempPanel.add( panel1, BorderLayout.SOUTH );
		
		// Setting the up frame window and adding the temporary Panel that has panel1 and panel2
		frame = new JFrame( "BorderLayout Demo" );
		frame.setVisible( true );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setSize( 700, 700 );
		frame.setLayout( new BorderLayout() );
		frame.add( tempPanel );
		frame.pack();
	}
	
	public static void main( String[] args ){
		new BorderLayoutDemo();
	}
}
