/**
 * @author ahsanzaman
 * CSC 201 - Unit 5
 * FlowLayout Demo
 * 
 * Algorithm:
 * 1. Declare and Initialize JFrame object frame
 * 2. Declare and Initialize JPanel object panel1
 * 3. Declare and Initialize JPanel object panel2
 * 4. Set title of the frame to "FlowLayout Demo"
 * 5. Set Size of the frame to 700 and 100
 * 6. Set the layout of the frame to FlowLayout
 * 7. Set the frame to be visible
 * 8. Set the default close operation to EXIT_ON_CLOSE
 * 9. Add the three JButton objects to the panel1
 * 10. Add the three JButton objects to the panel2
 * 11. Add panel1 and panel2 to the frame.
 * END
 */
import javax.swing.*;
import java.awt.*;
public class FlowLayoutDemo extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel panel1;
	private JPanel panel2;
	private JFrame frame;
	/**
	 * FlowLayoutDemo() - Default constructor frame is made and displayed
	 */
	public FlowLayoutDemo(){
		// Initializing JFrame object and setting its size, visibility and layout
		frame = new JFrame();
		frame.setTitle( "FlowLayout Demo" );
		frame.setSize( 700, 100 );
		frame.setLayout( new FlowLayout() );
		frame.setVisible( true );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		
		panel1 = new JPanel();
		panel1.add( new JButton( "Button 1" ) );
		panel1.add( new JButton( "Button 2" ) );
		panel1.add( new JButton( "Button 3" ) );
		
		panel2 = new JPanel();
		panel2.add( new JButton( "Button 4" ) );
		panel2.add( new JButton( "Button 5" ) );
		panel2.add( new JButton( "Button 6" ) );
		// Adding panel1 and panel2 to the JFrame object
		frame.add( panel1 );
		frame.add( panel2 );
		// Adjusting frame size according to the panels
		frame.pack();
	}
	
	public static void main( String[] args ){
		new FlowLayoutDemo();
	}
}
